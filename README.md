# Домашняя работа №3

1.	Добавить 2 окружения «preprod» и «production»
2.	Добавить отдельные deploy job для каждой среды
3.	Добавить переменную «$MyLogin» внутри .gitlab-ci.yml, которая будет меняться в зависимости от среды
4.	Добавить переменную «$MyPassword» не используя .gitlab-ci.yml, которая также будет меняться в зависимости от среды
5.	(*) Добавить скрипт .gitlab-ci.yml, который найдет все запущенные pipeline по названии ветки(ref) и остановит их.

Сделал по порядку. Пайплайн успешный (см. [коммит 1098035199](https://gitlab.com/RomanMenshikov92/homework-3/-/pipelines/1098035199))
