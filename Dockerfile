FROM busybox:latest

RUN echo "one" >> house.txt &&\
    echo "two" >> house.txt

# Установка необходимых утилит
RUN wget -O /bin/curl https://curl.haxx.se/download/curl-7.79.1.tar.xz 
    && tar -xvf /bin/curl 
    && rm -rf /bin/curl

RUN wget -O /bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 
    && chmod +x /bin/jq

